const logger = require('../view/logger');

/**
 * Handle user errors like misspelling and wrong action formats.
 *
 * @param  {Array}  args    CLI arguments.
 * @param  {Array}  actions Available actions in the app.
 * @return {void}         
 */
module.exports = (args, actions) => {
  const argsSize = args.length;
  const action = args[0];
  const errors = {
    actionFormatError: `
      Error: You need to provide one of these action formats:
      -----------------------------------------------
      01. \`add key value\`: add/update entry to dictionary.
      02. \`list\`: list all dictionary entries.
      03. \`get key\`: read target  dictionary entry.
      04. \`remove key\`: remove only target entry from dictionary.
      05. \`clear\`: remove all dictionary entries.
    `,
    addActionFormatError: '`add` action should take this format `add key value`',
    getActionFormatError: `
      \`get\` action should take this format \`get key\`
    `,
    removeActionFormatError: `
      \`remove\` action should take this format \`remove key\`
    `,
  };

  try {
    if (!action ||!actions.includes(action))
      throw new Error(errors.actionFormatError);

    if (action === 'add' && argsSize !== 3)
      throw new Error(errors.addActionFormatError);

    if (action === 'get' && argsSize !== 2)
      throw new Error(errors.getActionFormatError);

    if (action === 'remove' && argsSize !== 2)
      throw new Error(errors.removeActionFormatError);
  } catch (error) {
    logger('error', error);
  }
}
