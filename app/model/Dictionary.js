const fs = require('fs');

/**
 * User to initiate the model and provide the read/write methods.
 */
class Dictionary {
  constructor() {
    this.data = JSON.parse(fs.readFileSync(__dirname + '/dictionary.json'));
  }

  add(key, value) {
    this.data[key] = value;
    this.save(this.data);
  }

  list() {
    return JSON.stringify(this.data);
  }

  get(key) {
    return this.data[key];
  }

  remove(key) {
    try {
      if (!this.data[key])
        throw new Error('Error: Key not found!');

      delete this.data[key];
      this.save(this.data);
    } catch (error) {
      console.log(error);
    };
  }

  clear() {
    this.data = {};
    this.save(this.data);
  }

  save(dictionary) {
    fs.writeFileSync(__dirname + '/dictionary.json', JSON.stringify(dictionary), error => {
      console.log('done!')
      if (error) throw new Error(error);
    });
  }
}

module.exports = new Dictionary();
