const dict = require('../model/Dictionary');
const logger = require('../view/logger');
/**
 * Extract user provided action on the dictionary
 * and neccessary processes.
 *
 * @param  {Array} args CLI arguments.
 * @return {void}
 */
module.exports = args => {
  switch(args[0]) {
    case 'add':
      dict.add(args[1], args[2]);
      logger('log', `Successfully updated dictionary.`);
      break;
    case 'list':
      logger('log', dict.list());
      break;
    case 'get':
      logger('log', dict.get(args[1]));
      break;
    case 'remove':
      dict.remove(args[1]);
      logger('log', `Successfully removed entry.`);
      break;
    case 'clear':
      dict.clear();
      logger('log', `Successfully cleared dictionary.`);
      break;
  }
};
