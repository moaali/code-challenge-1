const handleError = require('./app/controllers/handleError');
const handleAction = require('./app/controllers/handleAction');

/**
 * Arguments list provided without `node store.js`.
 * @type {Array}
 * @example `node store.js get key` => args = ['key']
 */
const args = process.argv.slice(2);

/**
 * Allowable actions to use.
 * @type {Array}
 */
const actions = [
  'add',
  'clear',
  'remove',
  'list',
  'get',
];

// Handle all user errors in CLI.
handleError(args, actions);

// Handle user actions provided in CLI.
handleAction(args);
