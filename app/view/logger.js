/**
 * Represnts the view part which is the console.
 *
 * @param  {String} type     Type of console method.
 * @param  {String} feedback Message to print in the console.
 * @return {void}          
 */
module.exports = (type, feedback) => {
  console[type](feedback);
};
